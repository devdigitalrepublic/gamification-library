<?php

namespace Reciclare\Gamification;

class Productivity extends Gamification
{
    /**
     * Envia os dados para a api do gamification com o valor da produtividade do usuário
     * @param [string] $cpf
     * @param [int] $achievedGoal
     * @param [int] $goal
     * @param [string] $url
     */
    public function send($cpf, $achievedGoal, $goal)
    {
        $score = ($achievedGoal / $goal) * 100;
        $url = $this->request->getUrl() . '/api/listeners/productivity';
        $data = ['cpf' => $cpf, 'value' => $score];

        return $this->request->post($url, $data);
    }
}
