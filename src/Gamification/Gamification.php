<?php

namespace Reciclare\Gamification;

use Reciclare\Adapter\GamificableHttpAdapter;
use Reciclare\Http\GamificationRequest;
use Reciclare\Util;

class Gamification
{
    protected $request;

    public function __construct(GamificableHttpAdapter $adapter)
    {
        $this->setUpRequest($adapter);
    }

    public function setUpRequest($adapter)
    {
        $request = new GamificationRequest;
        $request->setAdapter($adapter);
        $request->credentials();
        $this->request = $request;
    }

    public function getClass()
    {
        $class = explode('\\', get_class($this));
        $class = array_pop($class);
        return $class;
    }

    public function __call($name, $arguments)
    {
        $data = $arguments[0];
        $event = Util::snakeCase($data['event'], '-');
        $method = Util::snakeCase($name, '-');
        $data['alias'] = $event . '-' . $method;
        $result = $this->request->event($data);
        return $result;
    }
}
