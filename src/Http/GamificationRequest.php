<?php

namespace Reciclare\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use Reciclare\Adapter\GamificableHttpAdapter;

class GamificationRequest
{
    private $client;
    private $adapter;
    public $accessToken;

    public function __construct()
    {
        $this->client = new Client;
    }

    public function setAdapter(GamificableHttpAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * Retornando token
     * @return $this->accessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function getUrl()
    {
        return $this->adapter->url;
    }

    /**
     * Setando token
     * @param [type] $accessToken
     * @return void
     */
    public function setAccesssToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * Método que faz a requisição para recuperação do token do usuário
     * @return void
     */
    public function credentials()
    {
        $url = $this->adapter->url . '/oauth/token';
        $request = $this->post($url, [
            'client_id' => $this->adapter->getClientId(),
            'client_secret' => $this->adapter->getClientSecret(),
            'grant_type' => 'client_credentials',
        ]);

        $request = json_decode($request);
        $this->setAccesssToken($request->access_token);
    }

    /**
     * Método que faz requisições do tipo post
     * @param [string] $url
     * @param [array] $data
     * @return $response
     */
    public function post($url, $data)
    {
        $response = $this->client->request('POST', $url, [
            'form_params' => $data,
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getAccessToken(),
            ],
        ]);

        return $response->getBody()->getContents();
    }

    /**
     * Método que faz o envio dos eventos que o usuários disparam
     * @param [type] $alias
     * @param [type] $cpf
     * @return $result
     */
    public function event($data)
    {
        try {
            $url = $this->getUrl() . '/api/listeners/event/' . $data['alias'];
            $result = $this->post($url, $data);
            $result = json_decode($result);
            return $result->success;
        } catch (ClientException $e) {
            return false;
        }

    }
}
