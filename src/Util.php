<?php

namespace Reciclare;

class Util
{
    public static function snakeCase($value, $separator = '_')
    {
        $snakeCase = strtolower(preg_replace('/(?<!^)[A-Z]/', $separator . '$0', $value));
        return $snakeCase;
    }
}
