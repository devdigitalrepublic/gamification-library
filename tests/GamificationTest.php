<?php

use Reciclare\Gamification\Productivity;

class GamificationTest extends \tests\TestModel
{
    public function testProductivity()
    {
        $cpf = '01005888590';
        $accessToken = $this->accessToken;

        $productivity = new Productivity($this->adapter);
        $this->assertNotEmpty($productivity->send($cpf, 90, 100));
    }
}
