<?php
/**
 * Created by PhpStorm.
 * User: digtal
 * Date: 05/12/18
 * Time: 10:52
 */
namespace tests;

use Reciclare\Adapter\GamificationAdapter;
use Reciclare\Http\GamificationRequest;

abstract class TestModel extends \PHPUnit\Framework\TestCase
{
    protected $accessToken;
    protected $baseUrl = 'http://localhost:8001';
    protected $adapter;
    protected $cpf;

    public function setUp()
    {
        $this->setUpAdapter();
        $this->getCredentials();
        $this->cpf = '01005888590';
    }

    public function setUpAdapter()
    {
        $clientId = 3;
        $clientSecret = '1BoyqHnoQXDJVA0HpqIYDiddSiiZeA9gJ72CeetB';

        $adapter = new GamificationAdapter($this->baseUrl);
        $adapter->setClientId($clientId);
        $adapter->setClientSecret($clientSecret);
        $this->adapter = $adapter;
    }

    public function getCredentials()
    {
        $request = new GamificationRequest;
        $request->setAdapter($this->adapter);
        $request->credentials();
        $this->accessToken = $request->getAccessToken();
    }
}